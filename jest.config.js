export default {
  clearMocks: true,
  testEnvironment: "jsdom",
  transform: {},
  moduleNameMapper: {
    vue$: "vue/dist/vue.cjs.js",
  },
};
