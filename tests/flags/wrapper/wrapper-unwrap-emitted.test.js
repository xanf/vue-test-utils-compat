import { jest, describe, afterEach, beforeEach, it, expect } from "@jest/globals";
import { defineComponent, h } from "vue";
import { installCompat, compatFlags } from "../../../src/index.js";
import { describeOption } from "../../helpers.js";

describeOption(compatFlags.WRAPPER_UNWRAP_EMITTED, () => {
  let VTU;

  const FakeComponent = defineComponent({
    props: ["demo"],
    render() {
      this.$emit("demo", this.demo);
      return h("div", this.demo);
    },
  });

  beforeEach(async () => {
    jest.resetModules();
    VTU = await import("@vue/test-utils");
  });

  let wrapper;

  afterEach(() => {
    wrapper.unmount();
  });

  const REFERENCE_PROP = {
    foo: "bar",
  };

  describe("when enabled", () => {
    beforeEach(() => {
      installCompat(VTU, { [compatFlags.WRAPPER_UNWRAP_EMITTED]: true });
      wrapper = VTU.mount(FakeComponent, { props: { demo: REFERENCE_PROP } });
    });

    it("emitted event should contain unwrapper prop", async () => {
      expect(wrapper.emitted("demo")[0][0]).toBe(REFERENCE_PROP);
      expect(wrapper.emitted().demo[0][0]).toBe(REFERENCE_PROP);
    });
  });

  describe("when disabled", () => {
    beforeEach(() => {
      installCompat(VTU, { [compatFlags.WRAPPER_UNWRAP_EMITTED]: false });
      wrapper = VTU.mount(FakeComponent, { props: { demo: REFERENCE_PROP } });
    });

    it("should return wrapped props inside emit", async () => {
      expect(wrapper.emitted().demo[0][0]).not.toBe(REFERENCE_PROP);
      expect(wrapper.emitted().demo[0][0]).toStrictEqual(REFERENCE_PROP);
      expect(wrapper.emitted("demo")[0][0]).not.toBe(REFERENCE_PROP);
      expect(wrapper.emitted("demo")[0][0]).toStrictEqual(REFERENCE_PROP);
    });
  });
});
