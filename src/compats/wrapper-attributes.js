export function install(VTU, compatConfig) {
  [VTU.config.plugins.DOMWrapper, VTU.config.plugins.VueWrapper].forEach((pluginHost) =>
    pluginHost.install((wrapper) => {
      const { attributes } = wrapper;
      return {
        attributes(attr) {
          const originalAttributes = attributes.call(wrapper, attr);

          if (attr === "value" && compatConfig.WRAPPER_ATTRIBUTES_VALUE) {
            return "value" in wrapper.element ? wrapper.element.value : originalAttributes;
          }

          if (attr === "disabled" && compatConfig.WRAPPER_ATTRIBUTES_DISABLED) {
            return typeof originalAttributes === "string" ? "disabled" : originalAttributes;
          }

          if (attr) {
            if (
              compatConfig.WRAPPER_ATTRIBUTES_FROM_ATTRS &&
              originalAttributes == null &&
              wrapper.vm?.$attrs?.[attr]
            ) {
              return wrapper.vm.$attrs[attr];
            }

            return originalAttributes;
          }

          const normalizedDisabled = typeof originalAttributes.disabled === "string" ? "disabled" : originalAttributes.disabled;
          const normalizedAttributes = { ...originalAttributes };
          if (compatConfig.WRAPPER_ATTRIBUTES_VALUE && "value" in wrapper.element) {
            normalizedAttributes.value = wrapper.element.value;
          }
          if (compatConfig.WRAPPER_ATTRIBUTES_DISABLED && "disabled" in originalAttributes) {
            normalizedAttributes.disabled = normalizedDisabled;
          }

          return normalizedAttributes;
        },
      };
    })
  );
}
