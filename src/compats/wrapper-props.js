export function install(VTU, compatConfig) {
  VTU.config.plugins.VueWrapper.install((wrapper) => {
    const { props } = wrapper;
    return {
      props(prop) {
        const originalProps = props.call(wrapper, prop);
        if (!compatConfig.WRAPPER_UNWRAP_PROPS && !compatConfig.WRAPPER_EXCLUDE_LISTENERS_FROM_PROPS) {
          return originalProps;
        }

        if (prop === undefined) {
          const normalizedProps = { ...originalProps };
          if (compatConfig.WRAPPER_EXCLUDE_LISTENERS_FROM_PROPS) {
            Object.keys(normalizedProps).forEach((key) => {
              if (key.startsWith("on")) {
                delete normalizedProps[key];
              }
            });
          }

          return new Proxy(normalizedProps, {
            get(target, key) {
              const value = target[key];
              // eslint-disable-next-line no-underscore-dangle
              return value?.__v_raw ?? value;
            },
          });
        }

        // eslint-disable-next-line no-underscore-dangle
        return originalProps?.__v_raw ?? originalProps;
      },
    };
  });
}
